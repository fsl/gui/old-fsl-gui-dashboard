import { useState, useEffect } from 'react'
import React from 'react'
import Box from '@mui/material/Box'
import { Card, CardContent, CardActions } from '@mui/material'
import {Button} from '@mui/material'
import { Container, CssBaseline, TextField, Typography, Grid } from '@mui/material'
import { createSocketClient } from "@fsljs"
import { getCommsConfig } from "@fsljs"

function GUICard(props) {
	const [cardVariant, setCardVariant] = useState('elevation')

	function mouseEnter(){
		setCardVariant('outlined')
	}

	function mouseLeave(){
		setCardVariant('elevation')
	}

	return (
		<Card sx={{
			width: '100%'
			}}
			variant={cardVariant}
			onMouseEnter={mouseEnter}
			onMouseLeave={mouseLeave}
			// onClick={() => {props.createWindow(props.gui.gui)}}
		>
			<CardContent sx={{
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center'
				}}>
				<Typography sx={{ fontWeight: 'bold' }}>{props.gui.title} </Typography>
			</CardContent>
			<CardContent sx={{
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				marginTop: '2px'
				}}>
				<Typography>{props.gui.description} </Typography>
			</CardContent>
			
			<CardActions>
				<Button
					variant='contained' 
					size='small' 
					style={{marginLeft: 'auto', marginRight: 'auto'}}
					onClick={() => {props.createWindow(props.gui.gui)}}>
					<Typography>
						Open	
					</Typography>
				</Button>
			</CardActions>
		</Card>
	)
}


export default function App() {
  const [guis, setGuis] = useState([])
	const [matchedGuis, setMatchedGuis] = useState([])
  const [socketClient, setSocketClient] = React.useState(null)

  function onGuiList(data){
    setGuis(data)
    setMatchedGuis(data)
  }

  function registerSocketListeners(socket){
    socket.on('guiList', onGuiList)
  }

	useEffect( () => {
		const comms = getCommsConfig()
    const socket = createSocketClient(comms.socketServerPort)
    registerSocketListeners(socket)
    setSocketClient(socket)
		socket.emit('guiList')
		// return a cleanup function when component unmounted
		return () => socket.disconnect();
	} , [])

	const title = "FSL dashboard"

	function createWindow(gui) {
    console.log('createWindow')
    if (socketClient !== null) {
      socketClient.emit('createWindow', {gui:gui})
    }
	}

	const handleSearch = (event) => {
		let searchTerm = event.target.value
		console.log(guis)
		let matches = guis.filter(gui => {
			return gui.title.toLowerCase().includes(searchTerm) || gui.description.toLowerCase().includes(searchTerm) || gui.keywords.toLowerCase().includes(searchTerm)
		})
			setMatchedGuis(matches)
	}

  return (
		<Container component="main">
			<CssBaseline enableColorScheme />
			<Box sx={{
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center'
				}}>
				<Typography variant='h5' sx={{
					margin: '20px'
					}}>
					{title}
				</Typography>
			</Box>
			<Box sx={{
				flexGrow: 1, 
				marginBottom: '40px'
				}}>
				<Grid container spacing={2}>
					<Grid item xs={12}>
						<TextField 
							name="search"
							label="search"
							type="text"
							sx={{
								width: '100%',
								marginBottom: '40px'
							}}
							onChange={handleSearch}
						>
						</TextField>
					</Grid>
					{matchedGuis.map((gui, index) => (
						<Grid item xs={12} sm={6} md={4} lg={4} key={index} style={{display: 'flex'}}>
							<GUICard gui={gui} createWindow={createWindow} />
						</Grid>
					))}
				</Grid>
			</Box>
		</Container>

    
  )
}
