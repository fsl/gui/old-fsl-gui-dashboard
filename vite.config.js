import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'
const fslGuiDevPath = process.env.FSL_GUI_DEV_PATH

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  base: './',
  root: '.',
  resolve: {
    alias: {
      '@components': path.join(fslGuiDevPath, 'components', 'src'), // expects index.js
      '@fsljs': path.join(fslGuiDevPath, 'fsljs', 'src'), // expects index.js
    },  
  },
})
